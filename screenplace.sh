#!/bin/bash                                                           
# -*- coding: UTF8 -*-                                                

dftScreen="eDP1"                                                      

screens=$(xrandr | grep " connected" | cut -f 1 -d ' ' | grep "HDMI1")
screen=$(echo $screens | head -n 1)                                   

case "$1" in                                                          
    "reset")                                                          
        xrandr --auto                                                 
        ;;                                                            
    "left")                                                           
        [ -z "$screen" ] && >&2 echo "No screen connected." && exit 1 
        xrandr --output "$screen" --left-of "$dftScreen" --auto       
        ;;                                                            
    "right")                                                          
        [ -z "$screen" ] && >&2 echo "No screen connected." && exit 1 
        xrandr --output "$screen" --right-of "$dftScreen" --auto      
        ;;                                                                
        >&2 echo -e "Missing argument. Usage:\n$0 [reset|left|right]" 
        ;;                                                            
esac                                                            

