#!/usr/bin/bash

CPASSWORDS="/home/bombar/.local/bin/cranspasswords"

export GPG_TTY="$(tty)" # Prevents pinentry from crashing due to bad tty
GETPASS="$CPASSWORDS --minimal inria"
USERNAME="mbombar"

VPN="/opt/cisco/anyconnect/bin/vpn"
VPN_NAME="INRIA"
VPN_URI="vpn.inria.fr/all"

service_start () {
    sudo systemctl start vpnagentd.service
}

service_stop () {
    sudo systemctl stop vpnagentd.service
}


tmpfile_create () {
    TMP_FILE=$(mktemp);
    export TMP_FILE;
    echo $USERNAME >> "$TMP_FILE"
    $GETPASS >> "$TMP_FILE"
}

tmpfile_delete () {
    rm -rf "$TMP_FILE"
}


help () {
    echo "Connect to vpn $VPN_NAME";
    echo "Syntax: vpn [connect | disconnect]";
    tmpfile_delete;
}

while getopts ":h" option; do
    case $option in
        h)
            help
            exit;;
        *)
            help
            exit;;
    esac
done

shift $(( OPTIND - 1 ));

# if [[ $EUID -ne 0 ]]; then
#     sudo -k;
#     exec sudo "$0" "$@";
# fi



case $1 in
    "connect")
        service_start;
        tmpfile_create;
        echo "TMP_FILE=$TMP_FILE";
        $VPN connect $VPN_URI -s < $TMP_FILE;
        tmpfile_delete;
        exit;
        ;;
    "disconnect")
        $VPN disconnect;
        service_stop;
        tmpfile_delete;
        exit;
        ;;
    *)
        help;
        exit;
        ;;
esac;
