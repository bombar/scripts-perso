#!/usr/bin/bash



VPN_LIST="INRIA,CWI"
VPN_SESSION="/home/bombar/.cache/vpn"

mkdir -p "$VPN_SESSION"
VPN_SESSIONFILE="$VPN_SESSION/session"
CURRENT_VPN=$(cat $VPN_SESSIONFILE 2&>>/dev/null);


which_vpn () {
    read -rp "Which VPN do you want to connect to ? " vpn;
    VPN=${vpn^^};
    if [[ "$VPN_LIST" != *"$VPN"* ]]; then
        echo "$vpn not allowed. You must choose from $VPN_LIST";
        exit;
    fi;
    echo "$VPN" > $VPN_SESSIONFILE;
}



help () {
    echo "Connect to any vpn among $VPN_LIST";
    echo "Syntax: vpn [VPN_NAME] [connect | disconnect]";
}

while getopts ":h" option; do
    case $option in
        h)
            help
            exit;;
        *)
            help
            exit;;
    esac
done



ARG1=${1^^}


case $ARG1 in
    "INRIA")
        echo "$ARG1" > $VPN_SESSIONFILE;
        vpn_inria "$2";
        exit;
        ;;
    "CWI")
        echo "$ARG1" > $VPN_SESSIONFILE;
        vpn_cwi "$2";
        exit;
        ;;
    "HELP")
        help;
        exit;
        ;;
    "CONNECT")
        # help;
        which_vpn;
        CURRENT_VPN=$(cat $VPN_SESSIONFILE);
        vpn "$CURRENT_VPN" "${1,,}";
        exit;
        ;;
    "DISCONNECT")
        if [ ! -s $VPN_SESSIONFILE ]; then
            echo "VPN already disconnected.";
            exit;
        fi;
        CURRENT_VPN=$(cat $VPN_SESSIONFILE);
        vpn "$CURRENT_VPN" "${1,,}";
        rm $VPN_SESSIONFILE;
        exit;
        ;;
    *)
        help;
        exit;
        ;;
esac;
