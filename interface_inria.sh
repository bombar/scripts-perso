#!/usr/bin/bash
#
#


##OLD Stuff
# WIRED_INTERFACE="eno2"
# MAC_PERSO="74:78:27:14:3d:fd"
# MAC_INRIA="74:78:27:8A:73:BA"

MAC_PERSO="CC:96:E5:9D:D7:26" ## Mac Dock Dell with pass thru
MAC_INRIA="74:78:27:8A:73:BA"

WIRED_INTERFACE="enxac91a19f51bd"
# WIRED_INTERFACE="enxcc96e59dd726"
#WIRED_INTERFACE="eth0" ## Màj 2022-10-31 ??


while getopts "i:" flag
do
    case "${flag}" in
        i) WIRED_INTERFACE="${OPTARG}";;
        \?) Help; exit;;
    esac
done;

Help () {
    echo "Update interface $WIRED_INTERFACE with the right mac address"
    echo
    echo "Syntax: interface_inria [-I interface] [up|down|h|help]"
    echo "options:"
    echo "up      Up $WIRED_INTERFACE with Inria mac address"
    echo "down    Reconfigure $WIRED_INTERFACE with usual mac address"
    echo "h|help  Display this Help message"
    echo "-i $INTERFACE  Use the specified wired interface. Default $WIRED_INTERFACE"
    echo
}

up_interface () {
    ip a flush $WIRED_INTERFACE;
    ip link set dev $WIRED_INTERFACE down;
    ip link set dev $WIRED_INTERFACE address $1;
    ip link set dev $WIRED_INTERFACE up;
}

if [[ $EUID -ne 0 ]]; then
    sudo -k;
    exec sudo "$0" "$@";
fi


while getopts ":h" option; do
    case $option in
        h)
            Help
            exit;;
    esac
done

shift $(( OPTIND - 1 ));

if [[ $1 == "up" ]]; then
    up_interface $MAC_INRIA;
    dhclient -v -I $WIRED_INTERFACE;
    exit;
else
    if [[ $1 == "down" ]]; then
        up_interface $MAC_PERSO;
    else
        Help;
        exit;
    fi
fi
