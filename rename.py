#!/usr/bin/python
#

from pathlib import Path
import sys
import argparse

def remove_space(fname, sep):
    """
    Rename a file by replacing all spaces by sep.
    """
    to_rename = Path(fname)
    target = fname.replace(' ', sep)
    to_rename.rename(target)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'filename',
        nargs='?',
        default=None,
        help="Name of the file to rename"
    )
    parser.add_argument(
        '-s', '--separator',
        action='store',
        default='_',
        help="Separator to use, default to _."
    )
    args = parser.parse_args()

    if args.filename is None:
        parser.print_help()
        sys.exit(0)

    remove_space(args.filename, args.separator)

if __name__=='__main__':
    main()
