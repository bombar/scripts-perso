#!/bin/bash


URL="https://gitlab.crans.org/bombar/Latex.git"

GIT_PATH="${HOME}/Git"

REPONAME="Latex"

MAIN_DIR="Main"

LATEX_PATH="${GIT_PATH}/${REPONAME}"

if [ ! -d ${LATEX_PATH} ]
then
    mkdir -P ${GIT_PATH}
    git clone ${URL} ${LATEX_PATH}
fi

ln -s ${LATEX_PATH}/${MAIN_DIR}/Makefile ./
cp ${LATEX_PATH}/${MAIN_DIR}/main.tex ./
cp -r ${LATEX_PATH}/${MAIN_DIR}/src/ ./
cp ${LATEX_PATH}/${MAIN_DIR}/.gitignore ./
