#!/usr/bin/env bash

BIBTEX="$HOME/Git/scripts-perso/Bibtex"
VENV="$BIBTEX/venv"


if [ ! -d "$VENV" ]; then
    python3 -m venv "$VENV" --system-site-packages
    source "$VENV/bin/activate"
    pip3 install -r "$BIBTEX/requirements.txt"
fi

source "$VENV/bin/activate";

$BIBTEX/biblio.py "$@"
