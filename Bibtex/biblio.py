#!/usr/bin/env python3

"""
Makes life easier with handling Bibtex files
"""
import argparse
import pathlib
import sys


try:
    import bibtexparser
except ModuleNotFoundError:
    print("Missing package. Please, install requirements.txt\n")
    exit(42)


def sortbibtex(filename, write=True, dry=False):
    """
    Sort a bibtex file
    """
    filename = filename.split('.bib')[0]
    db = bibtexparser.bibdatabase.BibDatabase()
    writer = bibtexparser.bwriter.BibTexWriter()
    with open(f'{filename}_sort.bib', 'w') as bibfile_sort:
        with open(f'{filename}.bib', 'r') as bibfile:
            bibDB = bibtexparser.load(bibfile)
            db.entries = list(sorted(bibDB.entries, key=lambda x: x['ID']))
            db.strings = bibDB.strings
            db.comments = bibDB.comments
            if dry:
                sys.stdout.write(writer.write(db))
            else:
                bibfile_sort.write(writer.write(db))
    bibfile.close()
    bibfile_sort.close()
    if write:
        path = pathlib.Path(f"{filename}_sort.bib")
        path.rename(f"{filename}.bib")


def _main():
    parser = argparse.ArgumentParser(
        description="Manage bibtex Files.")

    parser.add_argument('FILENAME', type=str, default=None,
                        help='Name of the bibtex database')

    parser.add_argument('-d', '--dry-run', action="store_true",
                        required=False, help='Do not actually modify \
                        anything but print to stdout (default False)')

    parser.add_argument('-w', '--write', action="store_true",
                        required=False, help='Write to the same \
                        bibtex file. Default uses filename_sorted')

    parser.add_argument(
        '-s', '--sort',
        action="store_true",
        required=False,
        help='Sort the bibtex DB.'
    )

    options = parser.parse_args()
    dry = options.dry_run
    write = options.write

    if options.FILENAME is None:
        parser.print_help()
        exit(1)

    filename = options.FILENAME

    if options.sort:
        sortbibtex(filename, write, dry)


if __name__ == "__main__":
    _main()
