#!/usr/bin/env python3


from itertools import filterfalse
from pathlib import Path
from shutil import rmtree

import os
import platform
import re
import sys

import argparse

# Global strings

__DEBUG__=False

INITRD="initrd.img-"
SYSTEMMAP="System.map-"
VMLINUZ="vmlinuz-"
CONFIG="config-"

PREFIXES = [INITRD, SYSTEMMAP, VMLINUZ, CONFIG]

HEADERPREFIX = "linux-headers-"
HEADERCOMMONSUFFIX = "-common"


BOOTPATH="/boot"
MODULESPATH="/lib/modules"
HEADERSPATH="/usr/src"


if __DEBUG__:
    DEBUGPATH="/home/bombar/TestKernel"

    BOOTPATH = DEBUGPATH + BOOTPATH
    MODULESPATH = DEBUGPATH + MODULESPATH
    HEADERSPATH = DEBUGPATH + HEADERSPATH



class MyParser(argparse.ArgumentParser):
    def error(self, message):
        sys.stderr.write(f'error: {message}\n\n')
        self.print_help(sys.stderr)
        sys.exit(2)



def sorting_key_kernels(kernelid):
    """
    """
    kernelid = kernelid.split('.') # 6.10.0-rc1 --> ['6', '10', '0-rc1']
    version = int(kernelid[0]) # 6
    patchlevel = int(kernelid[1]) # 10
    sublevel = int(kernelid[2].split('-')[0]) # 0
    extraversion = kernelid[2].removeprefix(str(sublevel)) # -rc1
    return (version, patchlevel, sublevel, extraversion)



def list_kernels(verbose=False):
    """
    List all available kernel IDs
    """
    kernelIDs = []
    for path in os.listdir(BOOTPATH):
        for prefix in PREFIXES:
            if re.match(prefix, path):
                kernelID = path.removeprefix(prefix)
                break
        if kernelID not in kernelIDs:
            kernelIDs.append(kernelID)
    for kernelID in os.listdir(MODULESPATH):
        if kernelID not in kernelIDs:
            if verbose:
                print(f"Kernel {kernelID} already removed but modules are still present.")
            kernelIDs.append(kernelID)

        if kernelID not in kernelIDs:
            if verbose:
                print(f"Kernel {kernelID} already removed, but headers are still present.")
            kernelIDs.append(kernelID)

    return list(sorted(kernelIDs, key=sorting_key_kernels))


def list_headers(verbose=False):
    """
    """
    headers = []
    for path in os.listdir(HEADERSPATH):
        if re.match(HEADERPREFIX, path):
            header = path.removeprefix(HEADERPREFIX)
            if header not in headers:
                headers.append(header)
    return list(sorted(headers, key=sorting_key_kernels))

def clean_kernel(kernelid, verbose=False):
    """
    kernelid: Name of the kernel
    """

    if kernelid == platform.uname().release:
        sys.stderr.write(f"You are trying to remove current kernel {platform.uname().release}. Aborting.\n")
        sys.exit(3)

    if verbose:
        print(f"Cleaning old kernel {kernelid}\n")


    paths = [Path(f"{BOOTPATH}/{prefix}{kernelid}") for prefix in PREFIXES]
    paths.append(Path(f"{MODULESPATH}/{kernelid}"))


    paths = list(filter(lambda x: x.exists(), paths))

    if len(paths) == 0:
        print(f"Kernel {kernelid} is unknown.")
        sys.exit(4)

    for path in paths:
        if path.is_file():
            if verbose:
                print(f"rm {path}")
            os.remove(path)
        elif path.is_dir():
            if verbose:
                print(f"rm -r{path}")
            rmtree(path)
            



    # if verbose:
    #     for prefix in PREFIXES:
    #     print(f"rm -r {MODULESPATH}/{kernelid}")
    # for prefix in PREFIXES:
    #     try:
    #         os.remove(f"{BOOTPATH}/{prefix}{kernelid}")
    #     except FileNotFoundError:
    #         continue
    # rmtree(f"{MODULESPATH}/{kernelid}", ignore_errors=True)


def check_remaining_headers():
    """
    Check if all variant of a kernel have been removed.
    """
    kernels = list_kernels(verbose=False)
    headers = list_headers(verbose=False)
    removable = []
    for header in headers:
        kernelID = header.split('-')[0]
        if list(filter(lambda x: x.startswith(kernelID), kernels)) == []:
            removable.append(header)
    for header in removable:
        print(f"You can safely remove {HEADERPREFIX}{header}.")



def main():

    parser = MyParser(
        description="Help cleaning kernels",
    )
    parser.add_argument(
        '-l', '--list',
        action='store_true',
        help='list the installed kernels',
    )

    parser.add_argument(
        '--force',
        action='store_true',
        required=False,
        help="Remove the kernel without further question",
    )

    parser.add_argument(
        '-r', '--remove',
        nargs='?',
        default=None,
        help="remove the listed kernels",
    )

    parser.add_argument(
        '-v', '--verbose',
        action='store_true',
        help="More verbose",
    )

    if len(sys.argv)==1:
        parser.print_help(sys.stderr)
        sys.exit(1)

    args = parser.parse_args()

    if args.list:
        print("*********************")
        print("* Installed kernels *")
        print("*********************\n")
        for kernel in list_kernels(args.verbose):
            print(f"- {kernel}")

        print("\n*********************")
        print("* Installed headers *")
        print("*********************\n")
        for header in list_headers(args.verbose):
            print(f"- {HEADERPREFIX}{header}")


    if args.remove:
        kernels = args.remove.split(',')
        if not args.force and len(kernels)>0:
            if len(kernels)>1:
                ans = input(f"Do you really want to delete kernels {' '.join(kernels)}? [y/N] ")
            else:
                ans = input(f"Do you really want to delete kernel {' '.join(kernels)}? [y/N] ")
            if ans.lower() not in ['y', 'yes']:
                exit()
        try:
            if args.verbose and len(kernels)>0:
                print("\n")
            for kernel in kernels:
                clean_kernel(kernel, args.verbose)
                if args.verbose:
                    print("\n")
            check_remaining_headers()
            print("Don't forget to run `sudo update-grub`.")
        except Exception as msg:
            print(f"\nThere has been an error in removing the kernels:\n{msg}.")





if __name__ == '__main__':

    if __DEBUG__:
        print("Running in Debug mode")
    main()

